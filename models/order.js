import moment from 'moment';

class Order {
  constructor(id, ownerId, status, items, totalAmount, date) {
    this.id = id;
    this.ownerId = ownerId;
    this.status = status;
    this.items = items;
    this.totalAmount = totalAmount;
    this.date = date;
  }

  get readableDate() {
    return moment(this.date).format('MMMM Do YYYY, hh:mm');
  }
}

export default Order;
