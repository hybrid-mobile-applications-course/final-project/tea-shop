class Product {
  constructor(
      id, ownerId, title, imageUrl, description, price, phone, address,
      latitude, longitude) {
    this.id = id;
    this.ownerId = ownerId;
    this.title = title;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
    this.phone = phone;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;

  }
}

export default Product;
