import React, {useCallback, useEffect, useState} from 'react';
import {Alert, Platform, StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

import HeaderButton from '../../components/UI/HeaderButton';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

const MapScreen = props => {
  const passedLocation = props.navigation.getParam("initialLocation");
  const readOnly = props.navigation.getParam("readOnly");
  const [pickedLocation, setPickedLocation] = useState(passedLocation);
  const mapRegion = {
    latitude: passedLocation ? passedLocation.latitude : 54.6872,
    longitude: passedLocation ? passedLocation.longitude : 25.2797,
    latitudeDelta: 0.0222,
    longitudeDelta: 0.0221
  };

  let markerCoordinate;

  const pickLocationHandler = event => {
    if (readOnly) {
      return;
    }
    setPickedLocation({
      latitude: event.nativeEvent.coordinate.latitude,
      longitude: event.nativeEvent.coordinate.longitude
    });
  };

  if (pickedLocation) {
    markerCoordinate = {
      latitude: pickedLocation.latitude,
      longitude: pickedLocation.longitude
    };
  }

  const savePickedLocationHandler = useCallback(() => {
    if (!pickedLocation) {
      Alert.alert(
          "No location selected",
          "You haven't placed marker anywhere",
          [{ text: "OK" }]
      );
      return;
    }
    props.navigation.navigate("Cart", {
      selectedLocation: pickedLocation
    });
  }, [pickedLocation]);

  useEffect(() => {
    props.navigation.setParams({ saveLocation: savePickedLocationHandler });
  }, [savePickedLocationHandler]);

  return (
      <MapView
          style={styles.map}
          region={mapRegion}
          onPress={pickLocationHandler}
      >
        {markerCoordinate && (
            <Marker title="My location" coordinate={markerCoordinate} />
        )}
      </MapView>
  );
};

MapScreen.navigationOptions = navigationData => {
  const saveFunction = navigationData.navigation.getParam("saveLocation");
  const readOnly = navigationData.navigation.getParam("readOnly");
  if (readOnly) {
    return {};
  }
  return {
    headerRight: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Save location"
              iconName={
                Platform.OS === "android" ? "md-thumbs-up" : "ios-thumbs-up"
              }
              onPress={saveFunction}
          />
        </HeaderButtons>
    )
  };
};

const styles = StyleSheet.create({
  map: {
    flex: 1
  }
});

export default MapScreen;
