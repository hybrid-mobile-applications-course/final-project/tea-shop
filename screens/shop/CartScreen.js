import React, {useCallback, useState} from 'react';
import {
  ActivityIndicator,
  Button,
  FlatList,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import Colors from '../../constants/Colors';
import CartItem from '../../components/shop/CartItem';
import Card from '../../components/UI/Card';
import * as cartActions from '../../store/actions/cart';
import * as ordersActions from '../../store/actions/orders';
import LocationSelector from '../../components/shop/LocationSelector';

const CartScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [location, setLocation] = useState();

  const locationSelectedHandler = useCallback(userLocation => {
    setLocation(userLocation);
  }, []);

  const cartTotalAmount = useSelector(state => state.cart.totalAmount);
  const cartItems = useSelector(state => {
    const transformedCartItems = [];
    for (const key in state.cart.items) {
      transformedCartItems.push({
        productId: key,
        productTitle: state.cart.items[key].productTitle,
        productPrice: state.cart.items[key].productPrice,
        quantity: state.cart.items[key].quantity,
        sum: state.cart.items[key].sum,
      });
    }
    return transformedCartItems.sort((a, b) =>
        a.productId > b.productId ? 1 : -1,
    );
  });
  const dispatch = useDispatch();

  const sendOrderHandler = async () => {
    setIsLoading(true);
    await dispatch(ordersActions.addOrder(cartItems, cartTotalAmount, location));
    setIsLoading(false);
  };

  return (
      <View style={styles.screen}>
        <FlatList
            data={cartItems}
            keyExtractor={item => item.productId}
            renderItem={itemData => (
                <CartItem
                    quantity={itemData.item.quantity}
                    title={itemData.item.productTitle}
                    amount={itemData.item.sum}
                    deletable
                    onRemove={() => {
                      dispatch(
                          cartActions.removeFromCart(itemData.item.productId));
                    }}
                />
            )}
        />
        <Card style={styles.summary}>
          <Text style={styles.summaryText}>
            Total:{' '}
            <Text style={styles.amount}>
              ${Math.round(cartTotalAmount.toFixed(2) * 100) / 100}
            </Text>
          </Text>
          {isLoading ? (
              <ActivityIndicator size="small" color={Colors.primary}/>
          ) : (
              <Button
                  color={Colors.royal}
                  title="Order Now"
                  disabled={cartItems.length === 0 || !location}
                  onPress={sendOrderHandler}
              />
          )}
        </Card>
        <View style={styles.location}>
          <LocationSelector
              navigation={props.navigation}
              onLocationSelected={locationSelectedHandler}
          />
        </View>

      </View>
  );
};

CartScreen.navigationOptions = {
  headerTitle: 'My Cart',
};

const styles = StyleSheet.create({
  screen: {
    margin: 20,
  },
  location:{
    marginTop: 20,
  },
  summary: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 20,
    padding: 10,
  },
  summaryText: {
    fontFamily: 'open-sans-bold',
    fontSize: 18,
  },
  amount: {
    color: Colors.primary,
  },
});

export default CartScreen;
