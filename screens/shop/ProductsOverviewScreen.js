import React, {useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Button,
  FlatList,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import HeaderButton from '../../components/UI/HeaderButton';
import ProductItem from '../../components/shop/ProductItem';
import * as productsActions from '../../store/actions/products';
import Colors from '../../constants/Colors';

import * as cartActions from '../../store/actions/cart';

const ProductsOverviewScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const products = useSelector(state => state.products.availableProducts);
  const dispatch = useDispatch();

  const loadProducts = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);
    try {
      await dispatch(productsActions.fetchProducts());
    } catch (err) {
      setError(err.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    setIsLoading(true);
    loadProducts().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, loadProducts]);

  useEffect(() => {
    const willFocusSubscription = props.navigation.addListener(
        'willFocus',
        () => {
          loadProducts();
          return () => {
            willFocusSubscription.remove();
          };
        },
        [loadProducts],
    );
  });

  const selectItemHandler = (id, title, phone) => {
    props.navigation.navigate('ProductDetail', {
      productId: id,
      productTitle: title,
      phone: phone,
    });
  };

  if (error) {
    console.log(error);
    return (
        <View style={styles.center}>
          <Text>
            Error, can't connect to database.
          </Text>
          <Button
              title="Retry"
              onPress={loadProducts}
              color={Colors.primary}/>
        </View>
    );
  }

  if (isLoading) {
    return (
        <View style={styles.center}>
          <ActivityIndicator size='large' color={Colors.primary}/>
        </View>
    );
  }

  if (!isLoading && products.length === 0) {
    return (
        <View style={styles.center}>
          <Text>
            No active listings.
          </Text>
        </View>
    );
  }

  return (
      <FlatList
          onRefresh={loadProducts}
          refreshing={isRefreshing}
          data={products}
          renderItem={
            itemData => <ProductItem
                imageUrl={itemData.item.imageUrl}
                title={itemData.item.title}
                price={itemData.item.price}
                onSelect={() => {
                  selectItemHandler(
                      itemData.item.id,
                      itemData.item.title,
                      itemData.item.phone);
                }}
            >
              <Button color={Colors.primary} title='More details'
                      onPress={() => {
                        selectItemHandler(
                            itemData.item.id,
                            itemData.item.title,
                            itemData.item.phone);
                      }}/>
              <Button color={Colors.primary} title="To Cart"
                      onPress={() => {
                        dispatch(cartActions.addToCart(itemData.item));
                      }}/>
            </ProductItem>
          }
      />
  );
};

ProductsOverviewScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Shop',
    headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Menu"
              iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
          />
        </HeaderButtons>
    ),

    headerRight: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Cart"
              iconName={Platform.OS === 'android' ? 'md-cart' : 'ios-cart'}
              onPress={() => {
                navData.navigation.navigate('Cart');
              }}
          />
        </HeaderButtons>
    ),

  };
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ProductsOverviewScreen;
