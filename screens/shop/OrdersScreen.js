import React, {useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Button,
  FlatList,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';

import HeaderButton from '../../components/UI/HeaderButton';
import OrderItem from '../../components/shop/OrderItem';
import * as ordersActions from '../../store/actions/orders';
import Colors from '../../constants/Colors';

const OrdersScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const orders = useSelector(state => state.orders.userOrders);
  const dispatch = useDispatch();

  const loadOrders = useCallback(async () => {
    setError(null);
    setIsRefreshing(true);
    try {
      await dispatch(ordersActions.fetchOrders());
    } catch (err) {
      setError(err.message);
    }
    setIsRefreshing(false);
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    setIsLoading(true);
    loadOrders().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, loadOrders]);

  useEffect(() => {
    const willFocusSubscription = props.navigation.addListener(
        'willFocus',
        () => {
          loadOrders();
          return () => {
            willFocusSubscription.remove();
          };
        },
        [loadOrders],
    );
  });

  if (error) {
    console.log(error);
    return (
        <View style={styles.center}>
          <Text>
            Error, can't connect to database.
          </Text>
          <Button
              title="Retry"
              onPress={loadOrders}
              color={Colors.primary}/>
        </View>
    );
  }

  if (isLoading) {
    return (
        <View style={styles.centered}>
          <ActivityIndicator size="large" color={Colors.primary}/>
        </View>
    );
  }

  if (!isLoading && orders.length === 0) {
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>You have no orders. Buy something</Text>
        </View>
    );
  }

  return (
      <FlatList
          onRefresh={loadOrders}
          refreshing={isRefreshing}
          data={orders}
          keyExtractor={item => item.id}
          renderItem={itemData => (
              <OrderItem
                  status={itemData.item.status}
                  amount={itemData.item.totalAmount}
                  date={itemData.item.readableDate}
                  items={itemData.item.items}
              />
          )}
      />
  );
};

OrdersScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Orders',
    headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
          <Item
              title="Menu"
              iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
              onPress={() => {
                navData.navigation.toggleDrawer();
              }}
          />
        </HeaderButtons>
    ),
  };
};

const styles = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default OrdersScreen;
