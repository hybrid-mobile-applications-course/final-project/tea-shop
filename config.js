import {
  apiKey,
  appId,
  authDomain,
  measurementId,
  messagingSenderId,
  projectId,
  realtimeDatabase,
  storageBucket,
  googleMapsApiKey,
} from '@env';

export default {
  apiKey,
  authDomain,
  projectId,
  storageBucket,
  messagingSenderId,
  appId,
  measurementId,
  realtimeDatabase,
  googleMapsApiKey,
};
