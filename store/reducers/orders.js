import {ADD_ORDER, SET_ORDERS} from '../actions/orders';
import Order from '../../models/order';

const initialState = {
  orders: [],
  userOrders: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ORDERS:
      return {
        orders: action.orders,
        userOrders: action.userOrders,
      };
    case ADD_ORDER:
      const newOrder = new Order(
          action.orderData.id,
          action.orderData.ownerId,
          action.orderData.status,
          action.orderData.items,
          action.orderData.amount,
          action.orderData.date,
          action.orderData.address,
          action.orderData.coordinates.latitude,
          action.orderData.coordinates.longitude

      );
      return {
        ...state,
        orders: state.orders.concat(newOrder),
      };
  }

  return state;
};
