import {SET_PRODUCTS} from '../actions/products';

const initialState = {
  availableProducts: [],
  userProducts: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return {
        availableProducts: action.availableProducts,
        userProducts: action.userProducts,
      };
  }
  return state;
};
