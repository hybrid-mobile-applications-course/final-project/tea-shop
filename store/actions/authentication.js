import {AsyncStorage} from 'react-native';

import config from '../../config';

export const AUTHENTICATE = 'AUTHENTICATE';
export const LOGOUT = 'LOGOUT';

let timer;

export const authenticate = (userId, token, displayName, timeToExpire) => {
  return dispatch => {
    dispatch(setLogOutTimer(timeToExpire));
    dispatch({
      type: AUTHENTICATE,
      userId: userId,
      token: token,
      displayName: displayName,
    });
  };
};

export const register = (email, password, displayName) => {
  return async dispatch => {
    const response = await fetch(
        `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${config.apiKey}`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: email,
            password: password,
            displayName: displayName,
            returnSecureToken: true,
          }),
        },
    );
    if (!response.ok) {
      const errorResponseData = await response.json();
      const errorMessage = errorResponseData.error.message;
      let customErrorMessage = 'Login failed';
      if (errorMessage === 'EMAIL_EXISTS') {
        customErrorMessage = 'This email is already registered';
      } else if (errorMessage === 'TOO_MANY_ATTEMPTS_TRY_LATER') {
        customErrorMessage = 'Too many failed attempts. Try again later.';
      }
      throw new Error(customErrorMessage);
    }
    const responseData = await response.json();

    dispatch(
        authenticate(
            responseData.localId,
            responseData.idToken,
            responseData.displayName,
            parseInt(responseData.expiresIn) * 1000,
        ),
    );
    const expirationDate = new Date(
        new Date().getTime() + parseInt(responseData.expiresIn) * 1000,
    );
    saveToken(responseData.idToken, responseData.localId, expirationDate);
  };
};

export const login = (email, password) => {
  return async dispatch => {
    const response = await fetch(
        `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${config.apiKey}`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: email,
            password: password,
            returnSecureToken: true,
          }),
        },
    );
    if (!response.ok) {
      const errorResponseData = await response.json();
      const errorMessage = errorResponseData.error.message;
      let customErrorMessage = 'Login failed';
      if (errorMessage === 'EMAIL_NOT_FOUND') {
        customErrorMessage = 'User does not exist';
      } else if (errorMessage === 'INVALID_PASSWORD') {
        customErrorMessage = 'Wrong password';
      } else if (errorMessage === 'USER_DISABLED') {
        customErrorMessage = 'Your account has been disabled';
      }
      throw new Error(customErrorMessage);
    }
    const responseData = await response.json();

    dispatch(
        authenticate(
            responseData.localId,
            responseData.idToken,
            responseData.displayName,
            parseInt(responseData.expiresIn) * 1000,
        ),
    );
    const expirationDate = new Date(
        new Date().getTime() + parseInt(responseData.expiresIn) * 1000,
    );
    saveToken(responseData.idToken, responseData.localId, expirationDate);
  };
};

export const logout = () => {
  clearTimer();
  AsyncStorage.removeItem('userToken');
  return {type: LOGOUT};
};

const clearTimer = () => {
  if (timer) {
    clearTimeout(timer);
  }
};

const setLogOutTimer = expirationTime => {
  return dispatch => {
    timer = setTimeout(() => {
      dispatch(logout());
    }, expirationTime);
  };
};

const saveToken = (token, userId, expirationDate) => {
  AsyncStorage.setItem(
      'userToken',
      JSON.stringify({
        token: token,
        userId: userId,
        expires: expirationDate.toISOString(),
      }),
  );
};
