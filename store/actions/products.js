import Product from '../../models/product';

import config from '../../config';

export const SET_PRODUCTS = 'SET_PRODUCTS';

export const fetchProducts = () => {
  return async (dispatch, getState) => {
    try {
      const userId = getState().authentication.userId;
      const token = getState().authentication.token;

      const response = await fetch(
          `${config.realtimeDatabase}listings.json?auth=${token}`);
      if (!response.ok) {
        throw new Error('Something went wrong');
      }

      const responseData = await response.json();
      const loadedProducts = [];

      for (const key in responseData) {
        loadedProducts.push(new Product(
            key,
            responseData[key].ownerId,
            responseData[key].title,
            responseData[key].imageUrl,
            responseData[key].description,
            responseData[key].price,
            responseData[key].phone,
        ));
      }

      dispatch({
        type: SET_PRODUCTS,
        availableProducts: loadedProducts,
        userProducts: loadedProducts.filter(
            product => product.ownerId === userId,
        ),
      });
    } catch (err) {
      throw err;
    }
  };
};
