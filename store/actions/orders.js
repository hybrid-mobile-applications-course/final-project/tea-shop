import Order from '../../models/order';

import config from '../../config';

export const ADD_ORDER = 'ADD_ORDER';
export const SET_ORDERS = 'SET_ORDERS';

export const fetchOrders = () => {
  return async (dispatch, getState) => {
    const userId = getState().authentication.userId;
    const token = getState().authentication.token;
    try {
      const response = await fetch(
          `${config.realtimeDatabase}orders.json?auth=${token}`,
      );

      if (!response.ok) {
        throw new Error('Something went wrong!');
      }

      const responseData = await response.json();
      const loadedOrders = [];

      for (const key in responseData) {
        loadedOrders.push(new Order(
            key,
            responseData[key].ownerId,
            responseData[key].status,
            responseData[key].cartItems,
            responseData[key].totalAmount,
            new Date(responseData[key].date),
        ));
      }

      dispatch({
        type: SET_ORDERS,
        orders: loadedOrders,
        userOrders: loadedOrders.filter(
            order => order.ownerId === userId,
        ),
      });
    } catch (err) {
      throw err;
    }
  };
};

export const addOrder = (cartItems, totalAmount, location) => {
  return async (dispatch, getState) => {
    const token = getState().authentication.token;
    const userId = getState().authentication.userId;
    const date = new Date();
    const status = 'Pending';
    const locationResponse = await fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${location.latitude},${location.longitude}&key=${config.googleMapsApiKey}`
    );
    const locationResponseData = await locationResponse.json();
    const address = locationResponseData.results[0].formatted_address;
    const response = await fetch(
        `${config.realtimeDatabase}orders.json?auth=${token}`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            ownerId: userId,
            status,
            cartItems,
            totalAmount,
            date: date.toISOString(),
            address,
            latitude: location.latitude,
            longitude: location.longitude,
          }),
        },
    );

    if (!response.ok) {
      throw new Error('Something went wrong!');
    }

    const responseData = await response.json();

    dispatch({
      type: ADD_ORDER,
      orderData: {
        id: responseData.name,
        ownerId: userId,
        status: status,
        items: cartItems,
        amount: totalAmount,
        date: date,
        address: address,
        coordinates: {
          latitude: location.latitude,
          longitude: location.longitude
        },
      },
    });
  };
};
