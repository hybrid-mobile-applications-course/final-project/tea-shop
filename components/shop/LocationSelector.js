import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  ActivityIndicator,
  Alert
} from "react-native";
import * as Location from "expo-location";
import * as Permissions from "expo-permissions";

import Colors from "../../constants/Colors";
import MapPreview from "./MapPreview";

const LocationSelector = props => {
  const [selectedLocation, setSelectedLocation] = useState();
  const [isFetching, setIsFetching] = useState(false);
  const selectedLocationManually = props.navigation.getParam(
      "selectedLocation"
  );

  const {onLocationSelected} = props;

  useEffect(() => {
    if (selectedLocationManually) {
      setSelectedLocation(selectedLocationManually);
      onLocationSelected(selectedLocationManually);
    }
  }, [selectedLocationManually, onLocationSelected]);

  const grantPermissions = async () => {
    const response = await Permissions.askAsync(Permissions.LOCATION);
    if (response.status !== "granted") {
      Alert.alert(
          "Permission not granted",
          "Please grant location permission to continue using this app.",
          [{ text: "OK" }]
      );
      return false;
    }
    return true;
  };

  const getLocationHandler = async () => {
    const hasPermission = await grantPermissions();
    if (!hasPermission) {
      return;
    }

    try {
      setIsFetching(true);
      const userLocation = await Location.getCurrentPositionAsync({
        timeout: 10000
      });
      setSelectedLocation({
        latitude: userLocation.coords.latitude,
        longitude: userLocation.coords.longitude
      });
      props.onLocationSelected({
        latitude: userLocation.coords.latitude,
        longitude: userLocation.coords.longitude
      });
    } catch {
      Alert.alert(
          "Can't get location.",
          "Manually pick location or try again later.",
          [{ text: "OK" }]
      );
    }
    setIsFetching(false);
  };

  const pickLocationHandler = () => {
    props.navigation.navigate("Map");
  };

  return (
      <View style={styles.locationSelectorView}>
        <MapPreview
            styles={styles.mapPreview}
            userLocation={selectedLocation}
            onPress={pickLocationHandler}
        >
          {isFetching ? (
              <ActivityIndicator size="large" color={Colors.primary} />
          ) : (
              <Text>Select location.</Text>
          )}
        </MapPreview>

        <View style={styles.locationButtonsView}>
          <Button
              title="Get Location"
              color={Colors.primary}
              onPress={getLocationHandler}
          />
          <Button
              title="Pick Location"
              color={Colors.primary}
              onPress={pickLocationHandler}
          />
        </View>
      </View>
  );
};

const styles = StyleSheet.create({
  locationSelectorView: {
    marginBottom: 14
  },
  mapPreview: {
    marginBottom: 12,
    width: "100%",
    height: 150,
    borderColor: Colors.grey,
    borderWidth: 1
  },
  locationButtonsView: {
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%"
  }
});

export default LocationSelector;
