import React from "react";
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import config from '../../config';
import Colors from "../../constants/Colors";

const MapPreview = props => {
  let mapPreviewUrl;

  if (props.userLocation) {
    mapPreviewUrl = `https://maps.googleapis.com/maps/api/staticmap?center=${props.userLocation.latitude},${props.userLocation.longitude}&zoom=15&size=400x200&maptype=roadmap&markers=color:black%7Clabel:X%7C${props.userLocation.latitude},${props.userLocation.longitude}&key=${config.googleMapsApiKey}`;
  }

  return (
      <TouchableOpacity
          onPress={props.onPress}
          style={{ ...styles.mapPreview, ...props.styles }}
      >
        {props.userLocation ? (
            <Image style={styles.mapImage} source={{ uri: mapPreviewUrl }} />
        ) : (
            props.children
        )}
      </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  mapPreview: {
    justifyContent: "center",
    alignItems: "center"
  },
  mapImage: {
    height: "100%",
    width: "100%"
  }
});

export default MapPreview;
